import dgl
import numpy as np
import pandas as pd
from tqdm import tqdm
import networkx as nx
from concurrent.futures import ProcessPoolExecutor, wait
from copy import deepcopy
import gc
from random import sample
from tqdm import tqdm


def getDataSetInfo(dataset):
    train_data_path = "../datasets/" + dataset + "/userRatingTrain.txt"
    test_data_path = "../datasets/" + dataset + "/userRatingTest.txt"
    train_data = []
    test_data = []
    num_user, num_item = 0, 0
    with open(train_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = list(np.array(line.strip('\n').split(" ")).astype(int))
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, list_[1])
                    train_data.append(list_)
                except Exception:
                    print(line)

    with open(test_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = list(np.array(line.strip('\n').split(" ")).astype(int))
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, list_[1])
                    test_data.append(list_)
                except Exception:
                    print(line)

    num_item = num_item + 1
    num_user = num_user + 1

    dic = {"num_user": num_user, "num_item": num_item, "train_data": train_data, "test_data": test_data}
    np.save("../datasets/" + dataset + "/dataInfo.npy", dic)
    print("finished: ", dataset)


def getDatasetInfo2(dataset):
    train_data_path = "../datasets/" + dataset + "/train.txt"
    test_data_path = "../datasets/" + dataset + "/test.txt"
    train_data = []
    test_data = []
    num_user, num_item = 0, 0
    with open(train_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = list(np.array(line.strip('\n').split(" ")).astype(int))
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, max(list_[1:]))
                    for item in list_[1:]:
                        train_data.append([list_[0], item, 1])
                except Exception:
                    print(line)

    with open(test_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = list(np.array(line.strip('\n').split(" ")).astype(int))
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, max(list_[1:]))
                    for item in list_[1:]:
                        test_data.append([list_[0], item, 1])
                except Exception:
                    print(line)

    num_item = num_item + 1
    num_user = num_user + 1

    dic = {"num_user": num_user, "num_item": num_item, "train_data": train_data, "test_data": test_data}
    np.save("../datasets/" + dataset + "/dataInfo.npy", dic)
    print("finished: ", dataset)


def sample_k(k=20, index=None, neighbours=None, user_hash=None, n=500):
    dis = {}
    neighbours = neighbours[:n].copy() if len(neighbours) > n else neighbours
    length = len(neighbours)
    for i in range(length):
        dis[neighbours[i]] = len(np.bitwise_xor(user_hash[index].astype(int), user_hash[neighbours[i]].astype(int)).nonzero()[0])
    dis_sort = np.array(sorted(dis.items(), key=lambda x: x[1]))[:, 0].copy()
    return dis_sort[0:k].copy()


def getNeighboursData(dataset, k=20):
    user_hash = np.load('../datasets/' + dataset + '/userHashCodes.npy')
    item_hash = np.load('../datasets/' + dataset + '/itemHashCodes.npy')
    user_file = "user_neighbours_" + str(k)
    item_file = "item_neighbours_" + str(k)
    data = np.load("../datasets/" + dataset + "/dataInfo.npy", allow_pickle=True).item()
    num_user = data['num_user']
    num_item = data['num_item']
    ratings = data['train_data'] + data['test_data']

    data = pd.DataFrame(ratings, columns=['user', 'item', 'rating'])
    data['user'] = data['user'].astype(int)
    data['item'] = data['item'].astype(int)
    data['item'] = data['item'].apply(lambda x: x + num_user)
    data = data[['user', 'item']].values.tolist()
    g = dgl.from_networkx(nx.Graph(data))
    print('We have %d nodes.' % g.number_of_nodes())
    print('We have %d edges.' % (g.number_of_edges() // 2))
    res = {}
    for index in tqdm(range(num_user)):
        temp = dgl.bfs_nodes_generator(g, index)
        t = []
        length = len(temp)
        for i in range(length):
            if i in [2, 4, 6]:  # 取三阶邻居
                t.extend(np.array(temp[i]))
        if t and len(t) > k:
            t = sample_k(k, index, t, user_hash)
        res[index] = t

    np.save("../datasets/" + dataset + "/" + user_file + ".npy", res)
    res = {}
    for index in tqdm(range(num_user, num_user + num_item)):
        temp = dgl.bfs_nodes_generator(g, index)
        t = []
        length = len(temp)
        for i in range(length):
            if i in [2, 4, 6]:  # 取三阶邻居
                tp = np.array(temp[i])
                tp -= num_user
                t.extend(tp)
                del tp
        if t and len(t) > k:
            t = sample_k(k, index - num_user, t, item_hash)
        res[index - num_user] = t

    np.save("../datasets/" + dataset + "/" + item_file + ".npy", res)
    print("finished: ", dataset)


k = 20
datasets = ['amazon-book', 'gowalla']
# getNeighboursData(datasets[0], k)
tasks = []
executor = ProcessPoolExecutor(max_workers=1)
for dataset in datasets:
    getDatasetInfo2(dataset)
    # tasks.append(executor.submit(getDatasetInfo2, dataset))
# executor.shutdown()

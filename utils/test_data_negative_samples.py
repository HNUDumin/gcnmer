from random import sample

import dgl
import numpy as np
from concurrent.futures import ProcessPoolExecutor, wait
from tqdm import tqdm


def getNegativeSamples(dataset, K=100):
    user_file = 'userRatingNegatives_random_' + str(K)
    data = np.load("../datasets/" + dataset + "/dataInfo.npy", allow_pickle=True).item()
    num_user = data['num_user']
    num_item = data['num_item']
    train_data_pair, test_data_pair = data['train_data'], data['test_data']

    res = {}
    print(num_user, num_item)
    print("开始随机负采样")
    user_items = {}
    for (user, item) in train_data_pair + test_data_pair:
        user_items.setdefault(user, []).append(item)
    for index in tqdm(range(num_user)):
        neg_items = list(set(range(num_item)) - set(user_items[index]))
        if isinstance(K, float):
            res[index] = sample(neg_items, int(len(neg_items) * K))
        else:
            res[index] = sample(neg_items, K)
    np.save('../datasets/' + dataset + '/' + user_file + '.npy', res)
    print(dataset + ', Done!')


datasets = ['ml-100k', 'book-crossing', 'douban-movie', 'epinions', 'ml-1m']
tasks = []
executor = ProcessPoolExecutor(max_workers=6)
for dataset in tqdm(datasets):
    tasks.append(executor.submit(getNegativeSamples, dataset, 1000))
wait(tasks)
executor.shutdown()

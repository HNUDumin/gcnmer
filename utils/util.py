# encoding:utf-8

import heapq

import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import torch
from sklearn.manifold import TSNE
from tqdm import tqdm


class Helper(object):
    """
    工具类
    """

    def __init__(self, config):
        self.config = config

    def evaluate_model(self, model, testRatings, graph, epoch_id):
        """
        Evaluate the performance (Hit_Ratio, NDCG) of top-K recommendation
        Return: score of each test rating.
        """
        hits, ndcgs, hits_hash, ndcgs_hash = [], [], [], []
        for idx in tqdm(range(len(testRatings)), "evaluation epoch: {0}".format(str(epoch_id))):
            hr, ndcg, hr_hash, ndcg_hash = self.eval_one_rating(model, testRatings, idx, graph)
            hits.append(hr)
            ndcgs.append(ndcg)
            hits_hash.append(hr_hash)
            ndcgs_hash.append(ndcg_hash)
        return hits, ndcgs, hits_hash, ndcgs_hash

    def eval_one_rating(self, model, testRatings, idx, graph):
        rating = testRatings[idx]
        items = rating[2:]
        user = rating[0]
        pos_item = rating[1]
        items.append(pos_item)
        users_var = torch.LongTensor([user]).repeat(len(items)).to(self.config.gpu)
        items_var = torch.LongTensor(items).to(self.config.gpu)

        # 嵌入向量
        map_item_score = {}
        u_g_embeddings, pos_i_g_embeddings, _ = model(graph, users_var, items_var, [])
        predictions = model.rating(u_g_embeddings, pos_i_g_embeddings).detach().cpu().numpy()[0]
        for i in range(len(items)):
            item = items[i]
            map_item_score[item] = predictions[i]
        hrs, ndcgs = [], []
        for k in self.config.topK:
            rankList = heapq.nlargest(k, map_item_score, key=map_item_score.get)  # 找出集合中最大的N个元素
            hr = self.getHitRatio(rankList, pos_item)
            ndcg = self.getNDCG(rankList, pos_item)
            hrs.append(hr)
            ndcgs.append(ndcg)

        # 哈希
        map_item_score = {}
        user_hash = torch.sign(u_g_embeddings).detach().cpu().numpy()
        item_hash = torch.sign(pos_i_g_embeddings).detach().cpu().numpy()
        predictions = self.hangmingSimilarity(user_hash, item_hash)
        for i in range(len(items)):
            item = items[i]
            map_item_score[item] = predictions[i]
        hrs_hash, ndcgs_hash = [], []
        for k in self.config.topK:
            rankList = heapq.nlargest(k, map_item_score, key=map_item_score.get)  # 找出集合中最大的N个元素
            hr = self.getHitRatio(rankList, pos_item)
            ndcg = self.getNDCG(rankList, pos_item)
            hrs_hash.append(hr)
            ndcgs_hash.append(ndcg)

        return hrs, ndcgs, hrs_hash, ndcgs_hash

    def getHitRatio(self, rankList, gtItem):
        for item in rankList:
            if item == gtItem:
                return 1
        return 0

    def getNDCG(self, rankList, gtItem):
        for i in range(len(rankList)):
            item = rankList[i]
            if item == gtItem:
                return math.log(2) / math.log(i + 2)
        return 0

    def visualization(self, user_data, item_data, filename='visualiztion.png'):
        if isinstance(item_data, dict):
            item_data = [i for i in item_data.values()]
            user_data = [i for i in user_data.values()]
        else:
            item_data = item_data.cpu().detach().numpy()
            user_data = user_data.cpu().detach().numpy()
        tsne = TSNE(n_components=2, init='pca', random_state=0)
        item_result = tsne.fit_transform(item_data)
        user_result = tsne.fit_transform(user_data)
        x_user = []
        x_item = []
        y_user = []
        y_item = []
        for i in range(user_result.shape[0]):
            x_user.append(user_result[i][0])
            y_user.append(user_result[i][1])

        for i in range(item_result.shape[0]):
            x_item.append(item_result[i][0])
            y_item.append(item_result[i][1])
        plt.scatter(x_user, y_user, c='red')
        plt.scatter(x_item, y_item, c='blue')
        plt.xticks()
        plt.yticks()
        plt.title("visualization of embeddings")
        plt.legend()
        plt.savefig(filename)
        plt.close()

    def hangmingSimilarity(self, user_hash, item_hash):
        res = []
        for i in range(len(item_hash)):
            res.append(1 / (len(np.bitwise_xor(user_hash.astype(int), item_hash[i].astype(int)).nonzero()[0]) + 1))
        return res

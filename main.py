# encoding:utf-8
import warnings

warnings.filterwarnings("ignore")
import torch
import numpy as np
import time
from tqdm import tqdm
from config import Config
from model.sgcnhr import SGCNHR
from utils.util import Helper
from dataset import Dataset
import matplotlib.pyplot as plt
import os
from parser import parse_args


def training(model, graph, train_loader, epoch_id, optimizer):
    losses = []
    for batch_id, (u, pi_ni) in tqdm(enumerate(train_loader), "training epoch: {0}".format(str(epoch_id))):
        users = u
        pos_items = pi_ni[:, 0]
        neg_items = pi_ni[:, 1]
        u_g_embeddings, pos_i_g_embeddings, neg_i_g_embeddings = model(graph, users, pos_items, neg_items)
        loss = model.create_bpr_loss(u_g_embeddings, pos_i_g_embeddings, neg_i_g_embeddings)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    loss = np.mean(losses)
    return loss


def evaluation(model, graph, helper, testRatings, epoch_id):
    hits, ndcgs, hits_hash, ndcgs_hash = helper.evaluate_model(model, testRatings, graph, epoch_id)
    hr, ndcg, hr_hash, ndcg_hash = np.mean(hits, axis=0), np.mean(ndcgs, axis=0), np.mean(hits_hash, axis=0), np.mean(ndcgs_hash, axis=0)
    return hr, ndcg, hr_hash, ndcg_hash


def draw(title, y_label, x_label, file_path, steps, metrics):
    plt.plot(steps, metrics)
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(file_path)


def draw_loss(epoch, file_path, losses):
    steps = [i for i in range(int(epoch))]
    draw(title='loss curve', y_label='loss value', x_label='epoch', file_path=file_path, steps=steps, metrics=losses)


def draw_metrics(epoch, file_path, hrs, ndcgs, hrs_hash, ndcgs_hash):
    steps = [i for i in range(int(epoch))]
    dic = {0: '5', 1: "10", 2: '20', 3: '50', 4: 100}
    for i in range(len(hrs[0])):
        draw(title='hr curve', y_label='hr value', x_label='epoch', file_path=file_path + "%s_hr.jpg" % dic[i], steps=steps, metrics=np.array(hrs)[:, i])
        draw(title='ndcg curve', y_label='ndcg value', x_label='epoch', file_path=file_path + "%s_ndcg.jpg" % dic[i], steps=steps, metrics=np.array(ndcgs)[:, i])
        draw(title='hr curve with hash', y_label='hr value', x_label='epoch', file_path=file_path + "%s_hr_hash.jpg" % dic[i], steps=steps, metrics=np.array(hrs_hash)[:, i])
        draw(title='ndcg curve with hash', y_label='ndcg value', x_label='epoch', file_path=file_path + "%s_ndcg_hash.jpg" % dic[i], steps=steps, metrics=np.array(ndcgs_hash)[:, i])


def save_result(filepath):
    with open(filepath, 'w+') as f:
        for item in result:
            f.write(item + '\n')
        f.close()


def save_model(model, path):
    torch.save(model, path)


def load_model(path):
    model = torch.load(path)
    return model


def saveEmbeddings(model, dataset):
    np.save("./datasets/" + dataset + "/" + "userEmbeddings.npy", model.user_embeddings.detach().cpu().numpy())
    np.save("./datasets/" + dataset + "/" + "itemEmbeddings.npy", model.item_embeddings.detach().cpu().numpy())
    np.save("./datasets/" + dataset + "/" + "userHashCodes.npy", torch.sign(model.user_embeddings).detach().cpu().numpy())
    np.save("./datasets/" + dataset + "/" + "itemHashCodes.npy", torch.sign(model.item_embeddings).detach().cpu().numpy())


if __name__ == '__main__':
    args = parse_args()
    config = Config(args)
    helper = Helper(config)
    dataset = Dataset(config)
    num_users, num_items = dataset.num_users, dataset.num_items
    print("num_users = %d, num_items = %d" % (num_users, num_items))
    print("avg_user_mean = %.2f, avg_item_user = %.2f" % (len(dataset.user_item_interactions) / num_users, len(dataset.user_item_interactions) / num_items))
    result = []
    print(args)
    result.append(args)
    best_hr = 0
    best_ndcg = 0
    base_path = "./results/save_models/" + config.dataset_path.split("/")[2]
    graph = dataset.graph
    graph = graph.to(config.gpu)
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    if config.use_pretrain_model and os.path.exists(base_path + "/best_model.pkl"):
        sgcnhr = load_model(base_path + "/best_model.pkl").to(config.gpu)
        print("加载预训练模型！")
    else:
        sgcnhr = SGCNHR(graph, config.embedding_size, config.embedding_size, config.drop_ratio, config.n_layer).to(config.gpu)

    losses = []
    optimizer = torch.optim.Adam(sgcnhr.parameters(), config.lr)
    hrs, ndcgs, hrs_hash, ndcgs_hash = [], [], [], []
    for epoch in range(config.epoch):
        # 训练
        t1 = time.time()
        loss = training(sgcnhr, graph, dataset.get_user_data_loader(config.batch_size), epoch, optimizer)
        losses.append(loss)
        print("Training time is: [%.2f s], loss is: [%.2f]" % (time.time() - t1, loss))

        # 评估
        t2 = time.time()
        hr, ndcg, hr_hash, ndcg_hash = evaluation(sgcnhr, graph, helper, dataset.user_testRatings, epoch)
        print("evaluation results: hr=%s, ndcg=%s, hr_hash=%s, ndcg_hash=%s" % (str(hr), str(ndcg), str(hr_hash), str(ndcg_hash)))

        best_hr = max(best_hr, hr[2])
        best_ndcg = max(best_ndcg, ndcg[2])
        result.append("Evaluation results is: " + "hr=" + str(hr) + ",ndcg=" + str(ndcg) + ",hr_hash=" + str(hr_hash) + ",ndcg_hash=" + str(ndcg_hash))

        # 保存最好的模型
        if not config.use_pretrain_model and best_hr <= hr[2] and best_ndcg <= ndcg[2]:
            print("保存最好模型")
            save_model(model=sgcnhr, path=base_path + '/best_model.pkl')
            saveEmbeddings(sgcnhr, config.dataset)

            # 数据可视化
            # helper.visualization(sgcnhr.user_embeds, sgcnhr.item_embeds, base_path + "/" + str(epoch) + "-visualization.png")

    print('best hr and ndcg of user are:', best_hr, best_ndcg)
    res_file_path = base_path + "/" + time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
    save_result(res_file_path + "_results.txt")
    draw_loss(config.epoch, res_file_path + "_loss.jpg", losses)
    draw_metrics(config.epoch, res_file_path, hrs, ndcgs, hrs_hash, ndcgs_hash)
    print("Done!")

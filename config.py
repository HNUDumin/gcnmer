import torch


class Config(object):
    """
    全局配置信息类
    """

    def __init__(self, args):
        self.dataset = args.dataset
        self.dataset_path = './datasets/' + self.dataset + '/'
        self.test_negative_file = self.dataset_path + 'userRatingNegatives_random.npy'
        self.user_neighbours_file = self.dataset_path + 'user_neighbours.npy'
        self.item_neighbours_file = self.dataset_path + 'item_neighbours.npy'
        self.embedding_size = int(args.embedding_size)
        self.epoch = int(args.epoch)
        self.num_negatives = int(args.num_negatives)
        self.batch_size = int(args.batch_size)
        self.lr = float(args.lr)
        self.drop_ratio = float(args.drop_ratio)
        self.topK = list(args.topK)
        self.n_layer = int(args.n_layer)
        self.use_neighbours_data = int(args.use_neighbours_data)
        self.k = int(args.k)
        self.use_pretrain_model = int(args.use_pretrain_model)
        self.gpu = 'cuda:0' if int(args.use_gpu) and torch.cuda.is_available() else 'cpu'

    def __str__(self):
        return "dataset=" + str(self.dataset) + \
               ", embedding_size=" + str(self.embedding_size) + \
               ", epoch=" + str(self.epoch) + \
               ", topK=" + str(self.topK) + \
               ", lr=" + str(self.lr) + \
               ", batch_size=" + str(self.batch_size) + \
               ", negative_num=" + str(self.num_negatives) + \
               ", drop_ratio=" + str(self.drop_ratio) + \
               ", n_layer=" + str(self.n_layer) + \
               ", test_negative_file=" + str(self.test_negative_file) + \
               ", user_neighbours_file=" + str(self.user_neighbours_file) + \
               ", item_neighbours_file=" + str(self.item_neighbours_file) + \
               ", use_neighbours_data=" + str(self.use_neighbours_data) + \
               ", k=" + str(self.k) + \
               ", use_pretrain_model=" + str(self.use_pretrain_model) + \
               ", gpu=" + str(self.gpu)

python main.py --dataset=ml-100k --use_neighbours_data=1 --batch_size=32 --k=20 &\
python main.py --dataset=ml-1m --use_neighbours_data=1 --batch_size=32 --k=20 &\
python main.py --dataset=epinions --use_neighbours_data=1 --batch_size=32 --k=20 &\
python main.py --dataset=douban-movie --use_neighbours_data=1 --batch_size=256 --k=20 &\
python main.py --dataset=book-crossing --use_neighbours_data=1 --batch_size=256 --k=20
# encoding:utf-8

import torch
import torch.nn as nn
import dgl.function as fn


class HeteroRGCNLayer(nn.Module):
    def __init__(self, in_size, out_size, etypes, drop_out=0.2):
        super(HeteroRGCNLayer, self).__init__()
        self.weight = nn.ModuleDict({
            name: nn.Linear(in_size, out_size) for name in etypes
        })
        self.dropout = nn.Dropout(drop_out)
        for key, item in self.weight.items():
            nn.init.normal_(item.weight)

    def forward(self, G, feat_dict):
        funcs = {}
        for srctype, etype, dsttype in G.canonical_etypes:
            Wh = self.dropout(self.weight[etype](feat_dict[srctype]))
            G.nodes[srctype].data['Wh_%s' % etype] = Wh
            funcs[etype] = (fn.copy_u('Wh_%s' % etype, 'm'), fn.mean('m', 'h'))
        G.multi_update_all(funcs, 'sum')
        return {ntype: G.nodes[ntype].data['h'] for ntype in G.ntypes}


class SGCNHR(nn.Module):
    def __init__(self, G, in_size, out_size, drop_out=0.2, n_layer=1, lmbd=1e-5):
        super(SGCNHR, self).__init__()
        embed_dict = {ntype: nn.Parameter(torch.Tensor(G.number_of_nodes(ntype), in_size), requires_grad=True)
                      for ntype in G.ntypes}
        for key, embed in embed_dict.items():
            nn.init.xavier_uniform_(embed)
        self.n_layer = n_layer
        self.lmbd = lmbd
        self.embed = nn.ParameterDict(embed_dict)
        self.layer = HeteroRGCNLayer(in_size, out_size, G.etypes, drop_out)

    def create_bpr_loss(self, users, pos_items, neg_items):
        pos_scores = (users * pos_items).sum(1)
        neg_scores = (users * neg_items).sum(1)

        mf_loss = nn.LogSigmoid()(pos_scores - neg_scores).mean()
        mf_loss = -1 * mf_loss

        regularizer = (torch.norm(users) ** 2 + torch.norm(pos_items) ** 2 + torch.norm(neg_items) ** 2) / 2
        emb_loss = self.lmbd * regularizer / users.shape[0]

        return mf_loss + emb_loss

    def rating(self, u_g_embeddings, pos_i_g_embeddings):
        return torch.matmul(u_g_embeddings, pos_i_g_embeddings.t())

    def forward(self, G, users, pos_items, neg_items):
        h_dict = self.layer(G, self.embed)
        for i in range(1, self.n_layer):
            h_dict = self.layer(G, self.h_dict)
        self.user_embeddings, self.item_embeddings = h_dict['user'], h_dict['item']
        return h_dict['user'][users, :], h_dict['item'][pos_items, :], h_dict['item'][neg_items, :]

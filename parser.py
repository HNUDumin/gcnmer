import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='参数解析器')
    parser.add_argument('--dataset', default='ml-100k', type=str, help='数据集名称')
    parser.add_argument('--embedding_size', default=64, type=int, help='嵌入向量长度')
    parser.add_argument('--epoch', default=10, type=int, help='学习率')
    parser.add_argument('--num_negatives', default=10, type=int, help='负样本个数')
    parser.add_argument('--batch_size', default=32, type=int, help='每批数据样本数')
    parser.add_argument('--lr', default=0.001, type=float, help='学习率')
    parser.add_argument('--drop_ratio', default=0.0, type=float, help='丢失率')
    parser.add_argument('--n_layer', default=1, type=int, help='GCN 堆叠层数')
    parser.add_argument('--use_neighbours_data', default=1, type=int, help='邻接节点数据类型')
    parser.add_argument('--topK', default=[5, 10, 20, 50, 100], type=list, help='前K个')
    parser.add_argument('--k', default=20, type=int, help='邻居节点个数')
    parser.add_argument('--use_pretrain_model', default=0, type=int, help='学习率')
    parser.add_argument('--use_gpu', default=0, type=int, help='是否使用GPU')
    return parser.parse_args()
